// jQuery
$(document).ready(function() {

  // Menu
  const menuBtn = $('#menu-button');
  const menu = $('#menu');

  menuBtn.on('click', function(e) {
    menu.hasClass('-active') ? menu.removeClass('-active') : menu.addClass('-active');
  })

  // Função vídeo  
  const videoCover = $('#video-cover');
  const videoPlayer = $('#video-player');

  videoCover.on('click', function(e) {
    const el = $(this)

    el.addClass('-inactive')
    videoPlayer.trigger('play')
    
  });

  // Consumo da API
  
  const allowCrossOrigin = "&origin=*"

$.ajax({
    url: 'https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=Alber%20Einstein' + allowCrossOrigin,
    success: function (result) {
      // return data to html
      $('#wiki').find('.extract').html(result.query.pages[736].extract);
    }
  })

  // Menu Accordion

  const myAccordion = $('.accordion');

  myAccordion.on('click', '.item',function (e) {
    if ($(this).hasClass('-active')) {
      $(this).removeClass('-active')
    } else{
      // $('.accordion .item').removeClass('-active')
      $(this).addClass('-active')
    }
})

  // Modal

  // Open

  const btnModal = $('#button-modal');

  btnModal.on('click', function(e) {
    const modal = $('.modal-wiki')
    let myClass = modal.attr('class')
    myClass = myClass + '-active'

    modal.removeClass().addClass(myClass)
  })

  // Close Modal

  const btnCloseModal = $('#close-modal')

  btnCloseModal.on('click', function () {
    const modal = $(this).parent();
    let myClass = modal.attr('class');
    myClass = myClass.replace('-active', '');    

    modal.removeClass().addClass(myClass);
  })  


});

// Vanilla JS

// window.onload = function() {
  //code
// };